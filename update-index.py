#! /usr/bin/env python

import os.path
import struct
import sys

exceptions = {}

if __name__ == '__main__':
    dir = sys.argv[1]
    jp_dir = sys.argv[2]

    jp = {}
    fp = open(os.path.join(jp_dir, 'index.bin'), 'rb')
    while True:
        s = fp.read(0x10)
        if not s:
            break
        id, size, flags, unk = struct.unpack('>IIII', s)
        jp[id] = size, flags
    fp.close()

    fp = open(os.path.join(dir, 'index.bin'), 'rb')

    frags = []
    while True:
        s = fp.read(0x10)
        if not s:
            break

        id, size, flags, unk = struct.unpack('>IIII', s)
        if not os.path.exists(os.path.join(dir, 'FILE_%08X' % id)):
            continue  # inexistant file, should not be in the index

        if flags == 0:
            size = os.path.getsize(os.path.join(dir, 'FILE_%08X' % id))
        elif id in exceptions:
            size, flags = exceptions[id]
        elif id & 0xFFFFFF00 in (0x60002000, 0x60002100):
            size, flags = jp[id]

        frags.append(struct.pack('>IIII', id, size, flags, unk))

    fp.close()
    fp = open(os.path.join(dir, 'index.bin'), 'wb')
    fp.write(''.join(frags))
    fp.close()
