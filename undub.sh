#! /bin/bash
#
# undub.sh
# Main undub script. Calls the other tools from this project to create an
# undubbed version of Baten Kaitos from an US ISO and a JP ISO.
#
# Copyright (C) 2011 Pierre Bourdon <delroth@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

d=$(dirname "$0")
this=$(basename "$0")

die() {
    echo >&2 "error: $1"
    exit 1
}

copy_files() {
    pat=$1
    dst=$2

    for f in "$pat"*; do
        echo -ne "\rCopying: $f"
        cp "$f" "$dst"
    done
}

set_done() {
    echo -e "\rDone!\e[K"
}

echo "$this: Baten Kaitos undubbing script"

# Get arguments
if [ "$#" -ne 3 ]; then
    echo >&2 "error: not enough arguments"
    echo >&2 "usage: $this <us-dir> <jp-dir> <out-dir>"
    exit 1
fi

usdir=$1
jpdir=$2
outdir=$3

# Check if the arguments are valid

[ -d "$usdir" ] || die "$usdir does not exist"
[ -d "$jpdir" ] || die "$jpdir does not exist"

[ -e "$outdir" ] && die "$outdir already exists"

# Find Python2
if which python2 &>/dev/null; then
    PYTHON=python2
else
    PYTHON=python
fi

$PYTHON -V &>/dev/null || die "cannot find Python 2.x"

# Pre-run checks done. Let's start.
cat <<EOF
If you distribute an undubbed version of Baten Kaitos (that's illegal, don't do
it!), please consider crediting my work by mentioning my name (delroth) and/or
linking to this project page or to my blog (http://blog.delroth.net/) :-)

EOF

mkdir -p "$outdir"

echo "copying US system files"
cp -R "$usdir/&&systemdata" "$outdir"
cp "$usdir/index.bin" "$outdir"
cp "$usdir/opening.bnr" "$outdir"
cp "$usdir/debug_font.tpl" "$outdir"
set_done

echo "copying US data files"
copy_files "$usdir/FILE_" "$outdir"
set_done

echo "replacing battle voices"
copy_files "$jpdir"/FILE_700040 "$outdir"
copy_files "$jpdir"/FILE_700041 "$outdir"
copy_files "$jpdir"/FILE_700042 "$outdir"
set_done

echo "replacing Mizuti song"
cp "$jpdir"/FILE_70004FB9 "$outdir"
set_done

echo "replacing battle animations"
copy_files "$jpdir"/FILE_600020 "$outdir"
copy_files "$jpdir"/FILE_600021 "$outdir"
set_done

echo "generating story voices replacements"
$PYTHON "$d/copy-voices.py" "$usdir" "$jpdir" "$outdir" "$d/substitute-voices"

echo "regenerating the index.bin file"
$PYTHON "$d/update-index.py" "$outdir" "$jpdir"

echo "changing the game name"
$PYTHON "$d/edit-banner.py" "$outdir/opening.bnr" "Baten Kaitos - undubbed"

echo "------------------------"
echo "All done! Use a tool like GC Rebuilder to build an ISO from the output"
echo "directory (read the README for more infos)."

exit 0
