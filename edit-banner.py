#! /usr/bin/env python

import sys

if __name__ == '__main__':
    fp = open(sys.argv[1], "r+b")
    game_name = sys.argv[2]

    fp.seek(0x1820)
    fp.write(game_name)

    fp.seek(0x1860)
    fp.write(game_name)
