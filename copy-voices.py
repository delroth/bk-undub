#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path
import struct
import sys

NOT_IN_JP = [
    0x06640000,     # In FILE_500041B4
    0x0A260000,     # In FILE_500041EB
    0x0D0B0000,     # In FILE_50004220
    0x12590000,     # In FILE_30004278
    0x127A0000,     # In FILE_3000427A
    0x127C0000,     # In FILE_3000427A
    0x12890000,     # In FILE_3000427C
    0x16AC0000,     # In FILE_300042C7
    0x19A60000,     # In FILE_300042EF
]

class CSL:
    def __init__(self):
        self.parts = []

    def parse(self, fp):
        fp.seek(0)
        magic = struct.unpack('4s', fp.read(4))[0]
        if magic != b'CSL ':
            raise ValueError("not a valid CSL file (bad magic)")

        parts_off = []
        nparts = struct.unpack('>I', fp.read(4))[0]
        for i in range(nparts):
            parts_off.append(struct.unpack('>I', fp.read(4))[0])

        fp.seek(0, 2)
        parts_off.append(fp.tell())

        for (part_start, part_end) in zip(parts_off, parts_off[1:]):
            fp.seek(part_start)
            self.parts.append(list(fp.read(part_end - part_start)))

        return self

    def write(self, fp):
        fp.write(b'CSL ')
        fp.write(struct.pack('>I', len(self.parts)))

        start_off = 0x800
        parts_off = []
        for part in self.parts:
            parts_off.append(start_off)
            start_off += len(part)
            start_off += 0x20 - (start_off % 0x20) # alignment

        for off in parts_off:
            fp.write(struct.pack('>I', off))

        for (part, off) in zip(self.parts, parts_off):
            fp.seek(off)
            fp.write(''.join(part))

def get_uint32(csf, i):
    return struct.unpack('>I', ''.join(csf[i:i+4]))[0]

def set_uint32(csf, i, n):
    n = struct.pack('>I', n)
    csf[i:i+4] = n

def get_song_id(csf):
    return get_uint32(csf, 0x28)

def replace_song_id(csf, id):
    set_uint32(csf, 0x28, id)

def set_nnibbles(csf, nibbles):
    set_uint32(csf, 0x90, nibbles)

def set_size(csf, size):
    set_uint32(csf, 0x04, size)
    set_uint32(csf, 0x0C, size - 0xC0)

def remove_audio_data(csf):
    set_nnibbles(csf, 0)
    set_size(csf, 0x200)
    csf[0xA0:] = ["\0"] * (0x200 - 0xA0)

def audio_data_from_dsp(csf, fp):
    fp.seek(0x4)
    nnibbles = struct.unpack('>I', fp.read(4))[0]

    fp.seek(0x1C)
    coeffs = fp.read(0x20)

    fp.seek(0x60)
    data = fp.read()

    set_nnibbles(csf, nnibbles)
    csf[0xA0:0xC0] = coeffs
    csf[0xC0:] = data
    set_size(csf, len(csf))

def combine(fn, us_f, jp_f, out_f, repl_dir, no_repl):
    us = CSL().parse(us_f)
    jp = CSL().parse(jp_f)
    out = CSL()

    for i, us_part in enumerate(us.parts):
        if get_song_id(us_part) in NOT_IN_JP:
            jp.parts.insert(i, us_part[:])
            remove_audio_data(jp.parts[i])

    for jp_part, us_part in zip(jp.parts, us.parts):
        replace_song_id(jp_part, get_song_id(us_part))
        out.parts.append(jp_part)

    for part in us.parts[len(jp.parts):]:
        id = get_song_id(part)
        rfn = os.path.join(repl_dir, "%08X.dsp" % id)

        if os.path.exists(rfn):
            audio_data_from_dsp(part, open(rfn, 'rb'))
        else:
            no_repl.append((fn, rfn))
            remove_audio_data(part)

        out.parts.append(part)

    out.write(out_f)

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print >>sys.stderr, "usage: %s <us> <jp> <undub> <repl>" % sys.argv[0]
        sys.exit(1)

    us_dir = sys.argv[1]
    jp_dir = sys.argv[2]
    out_dir = sys.argv[3]
    repl_dir = sys.argv[4]
    no_repl = []

    for fn in os.listdir(us_dir):
        if not fn.startswith('FILE_50004') \
                and not fn.startswith('FILE_3000'):
            continue  # not a voice file

        print "\rCombining: %s" % fn,
        sys.stdout.flush()

        us_f = open(os.path.join(us_dir, fn), 'rb')
        jp_f = open(os.path.join(jp_dir, fn), 'rb')
        out_f = open(os.path.join(out_dir, fn), 'wb')

        combine(fn, us_f, jp_f, out_f, repl_dir, no_repl)

    print "\rEverything combined in the %s directory!" % out_dir
    if no_repl:
        print "Missing replacements:"
        for (f, r) in no_repl:
            print "- %s in %s" % (r, f)
